/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tapadoo.alerter.sample.slice;

import com.tapadoo.alerter.*;
import com.tapadoo.alerter.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Environment;
import ohos.global.resource.RawFileDescriptor;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.*;

/**
 * 示例
 */
public class MainAbilitySlice extends AbilitySlice {

    private DependentLayout componentContainer;
    private String ttfPath;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        int flagTranslucentNavigation = WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION;
        //沉浸式
        Window window = getWindow();
        WindowManager.LayoutConfig attributes = window.getLayoutConfig().get();
        attributes.flags |= flagTranslucentNavigation;
        window.setLayoutConfig(attributes);

        componentContainer = (DependentLayout) findComponentById(ResourceTable.Id_dl_group);
        //默认
        Button btnDefalut = (Button) findComponentById(ResourceTable.Id_btn_alert1);
        btnDefalut.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .show();
            }
        });

        //点击事件
        Button btnOnClick = (Button) findComponentById(ResourceTable.Id_btn_alert2);
        btnOnClick.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setDuration(10000)
                        .setOnClickListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new ToastDialog(getContext()).setText("CLICK").show();
                            }
                        })
                        .show();
            }
        });

        //自定义图标
        Button btnCustomIcon = (Button) findComponentById(ResourceTable.Id_btn_alert3);
        btnCustomIcon.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                    Alerter.create(MainAbilitySlice.this, componentContainer)
                            .setCustomLeftIconBig(ResourceTable.Media_custom_icon_big)
                            .setCustomLeftIconSmall(ResourceTable.Media_custom_icon_small)
                            .setEnableCustomLeftIconAnim(true)
                            .setEnableIconAnim(true)
                            .setTitle("Alert Title")
                            .setText("Alert text...")
                            .setIconSize(500)
                            .show();
            }
        });


        //自定义背景色
        Button btnBackGround = (Button) findComponentById(ResourceTable.Id_btn_alert4);
        btnBackGround.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setDuration(5000)
                        .setBackgroundColorInt(0xffF99143)
                        .show();
            }
        });

        //Progress
        Button btnProgress = (Button) findComponentById(ResourceTable.Id_btn_alert5);
        btnProgress.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setDuration(5000)
                        .enableProgress(true)
//                        .setProgressColorInt(0xff2c9cac)
                        .show();
            }
        });

        //字体
        Button btnFONT = (Button) findComponentById(ResourceTable.Id_btn_alert6);
        btnFONT.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                copyTTF();
                Alert alert = Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setDuration(5000)
                        .setBackgroundColorInt(0xffF99143)
                        .show();
                Font.Builder builder = new Font.Builder(new File(ttfPath));
                alert.getText().setFont(builder.build());
                alert.getTitle().setFont(builder.build());
            }
        });

        //回调
        Button btnCallBack = (Button) findComponentById(ResourceTable.Id_btn_alert7);
        btnCallBack.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setDuration(5000)
                        .setOnShowListener(new OnShowAlertListener() {
                            @Override
                            public void onShow() {
                                new ToastDialog(getContext()).setText("show").show();
                            }
                        })
                        .setOnHideListener(new OnHideAlertListener() {
                            @Override
                            public void onHide() {
                                new ToastDialog(getContext()).setText("hide").show();
                            }
                        })
                        .show();
            }
        });

        //增加按钮
        Button btnAddButton = (Button) findComponentById(ResourceTable.Id_btn_alert8);
        btnAddButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setDuration(5000)
                        .addButton("YES", 0xffffffff, 0xffF99143, 200, 60, new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new ToastDialog(getContext()).setText("yes").show();
                            }
                        })
                        .addButton("NO", 0xffffffff, 0xffF99143, 200, 60, new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new ToastDialog(getContext()).setText("no").show();
                            }
                        })
                        .show();
            }
        });

        //自定义布局
        Button btnCustomLayout = (Button) findComponentById(ResourceTable.Id_btn_alert9);
        btnCustomLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer, ResourceTable.Layout_customer_layout)
                        .setDuration(5000)
                        .show();
            }
        });

        //VERBOSE
        Button btnVerbose = (Button) findComponentById(ResourceTable.Id_btn_alert10);
        btnVerbose.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("The alert scales to accommodate larger bodies of text. " +
                                "The alert scales to accommodate larger bodies of text. " +
                                "The alert scales to accommodate larger bodies of text.")
                        .show();
            }
        });

        //中间弹窗
        Button btnCenter = (Button) findComponentById(ResourceTable.Id_btn_alert11);
        btnCenter.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setContentGravity(DependentLayout.LayoutConfig.CENTER_IN_PARENT)
                        .show();
            }
        });

        //底部弹窗
        Button btnBottom = (Button) findComponentById(ResourceTable.Id_btn_alert12);
        btnBottom.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .setContentGravity(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM)
                        .show();
            }
        });

        //带右图标弹窗
        Button btnWithRightIcon = (Button) findComponentById(ResourceTable.Id_btn_alert13);
        btnWithRightIcon.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setCustomRightIconBig(ResourceTable.Media_custom_icon_big)
                        .setCustomRightIconSmall(ResourceTable.Media_custom_icon_small)
                        .setEnableCustomRightIconAnim(true)
                        .setEnableRightIcon(true)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .show();
            }
        });

        //只有右图标弹窗
        Button btnOnlyRightIcon = (Button) findComponentById(ResourceTable.Id_btn_alert14);
        btnOnlyRightIcon.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setCustomRightIconBig(ResourceTable.Media_custom_icon_big)
                        .setCustomRightIconSmall(ResourceTable.Media_custom_icon_small)
                        .setEnableCustomRightIconAnim(true)
                        .setEnableOnlyRightIcon(true)
                        .setEnableRightIcon(true)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .show();
            }
        });

        //右上角图标弹窗
        Button btnRightOnTopIcon = (Button) findComponentById(ResourceTable.Id_btn_alert15);
        btnRightOnTopIcon.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setCustomRightIconBig(ResourceTable.Media_custom_icon_big)
                        .setCustomRightIconSmall(ResourceTable.Media_custom_icon_small)
                        .setRightIconTop()
                        .setEnableCustomRightIconAnim(true)
                        .setEnableOnlyRightIcon(true)
                        .setEnableRightIcon(true)
                        .setEnableIconAnim(true)
                        .setText("The alert scales to accommodate larger bodies of text. " +
                                "The alert scales to accommodate larger bodies of text. " +
                                "The alert scales to accommodate larger bodies of text.")
                        .show();
            }
        });

        //只有文本
        Button btnOnlyText = (Button) findComponentById(ResourceTable.Id_btn_alert16);
        btnOnlyText.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setText("Alert text...")
                        .show();
            }
        });

        //带铃声
        Button btnSound = (Button) findComponentById(ResourceTable.Id_btn_alert17);
        btnSound.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                copyMp3File();
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setSoundUri(Uri.parse(getFilesDir() + File.separator + "ringtone.mp3"))
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .show();
            }
        });

        //SWIPE DISMISS
        Button btnSwipeDismiss = (Button) findComponentById(ResourceTable.Id_btn_alert18);
        btnSwipeDismiss.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .enableSwipeToDismiss()
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .show();
            }
        });

        //自定义标题内容颜色
        Button btnCustomTextColor= (Button) findComponentById(ResourceTable.Id_btn_alert19);
        btnCustomTextColor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Alert alert =  Alerter.create(MainAbilitySlice.this, componentContainer)
                        .setEnableIconAnim(true)
                        .setTitle("Alert Title")
                        .setText("Alert text...")
                        .show();
                alert.getTitle().setTextColor(new Color(0xffF99143));
                alert.getText().setTextColor(new Color(0xffcc0000));
            }
        });

    }


    private void copyTTF() {
        //写入外部
        RawFileEntry rawFileEntry = getResourceManager().getRawFileEntry("resources/rawfile/Pacifico-Regular.ttf");
        if (rawFileEntry != null) {
            int a = 10;
            try {
                Resource resource = rawFileEntry.openRawFile();
                File f = new File(getFilesDir() + File.separator + "aaaaaa.ttf");
                if (!f.exists()) {
                    FileOutputStream fos = new FileOutputStream(f);
                    byte[] b = new byte[1024];
                    int length = 0;
                    while ((length = resource.read(b)) != -1) {
                        fos.write(b, 0, length);
                    }
                    fos.close();
                }
                ttfPath = f.getPath();
            } catch (IOException e) {
                LogUtil.error(this.getClass().getName(),"copyTTF---error:"+e);
            }
        }
    }

    public void copyMp3File() {
        try {
            String filePath = getFilesDir() + File.separator + "ringtone.mp3";
            RawFileDescriptor rfd = getResourceManager().getRawFileEntry("resources/rawfile/ringtone.mp3").openRawFileDescriptor();
            long start = rfd.getStartPosition();
            long fileSize = rfd.getFileSize();
            InputStream inStream = new FileInputStream(rfd.getFileDescriptor()); //读入原文件
            inStream.skip(start);
            FileOutputStream fs = new FileOutputStream(filePath);
            byte[] buffer = new byte[(int) fileSize];
            inStream.read(buffer);
            fs.write(buffer);
            fs.close();
            inStream.close();
            rfd.close();
        } catch (IOException e) {
            LogUtil.error(this.getClass().getName(),"copyMp3File---error:"+e);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}

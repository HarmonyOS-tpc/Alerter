﻿# Alerter
	An  Alerting Library

## General
With simplicity in mind, the Alerter employs the builder pattern to facilitate easy integration into any app. A customisable Alert View is dynamically added to the Decor View of the Window, overlaying all content.	
	
## 概述
 * 支持原有的核心功能，动画没有原组件的体验好，这里是用鸿蒙的写法自己改写的。
 * 不支持进出场动画自定义设置。
 * 使用媒体文件格式支持:图片:png,jpg 音频:mp3。

## 演示
<img src="screenshot/alerter.gif" width="75%"/>

## 集成

```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:Alerter:1.0.0' 
```

## entry运行要求
   通过DevEco studio,并下载SDK
   将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）


### 示例

```java

                        //默认
                        Button btnDefalut = (Button) findComponentById(ResourceTable.Id_btn_alert1);
                        btnDefalut.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .show();
                            }
                        });

                        //点击事件
                        Button btnOnClick = (Button) findComponentById(ResourceTable.Id_btn_alert2);
                        btnOnClick.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setDuration(10000)
                                        .setOnClickListener(new Component.ClickedListener() {
                                            @Override
                                            public void onClick(Component component) {
                                                new ToastDialog(getContext()).setText("CLICK").show();
                                            }
                                        })
                                        .show();
                            }
                        });

                        //自定义图标
                        Button btnCustomIcon = (Button) findComponentById(ResourceTable.Id_btn_alert3);
                        btnCustomIcon.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                try {
                                    Alerter.create(MainAbilitySlice.this, componentContainer)
                                            .setCustomLeftIconBig(ResourceTable.Media_custom_icon_big)
                                            .setCustomLeftIconSmall(ResourceTable.Media_custom_icon_small)
                                            .setEnableCustomLeftIconAnim(true)
                                            .setEnableIconAnim(true)
                                            .setTitle("Alert Title")
                                            .setText("Alert text...")
                                            .setIconSize(500) 
                                            .show();
                                } catch (Exception e) {

                                }

                            }
                        });


                        //自定义背景色
                        Button btnBackGround = (Button) findComponentById(ResourceTable.Id_btn_alert4);
                        btnBackGround.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setDuration(5000)
                                        .setBackgroundColorInt(0xffF99143)
                                        .show();
                            }
                        });

                        //Progress
                        Button btnProgress = (Button) findComponentById(ResourceTable.Id_btn_alert5);
                        btnProgress.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setDuration(5000)
                                        .enableProgress(true)
                //                        .setProgressColorInt(0xff2c9cac)
                                        .show();
                            }
                        });

                        //字体
                        Button btnFONT = (Button) findComponentById(ResourceTable.Id_btn_alert6);
                        btnFONT.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                copyTTF();
                                Alert alert = Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setDuration(5000)
                                        .setBackgroundColorInt(0xffF99143)
                                        .show();
                                Font.Builder builder = new Font.Builder(new File(ttfPath));
                                alert.getText().setFont(builder.build());
                                alert.getTitle().setFont(builder.build());
                            }
                        });

                        //回调
                        Button btnCallBack = (Button) findComponentById(ResourceTable.Id_btn_alert7);
                        btnCallBack.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setDuration(5000)
                                        .setOnShowListener(new OnShowAlertListener() {
                                            @Override
                                            public void onShow() {
                                                new ToastDialog(getContext()).setText("show").show();
                                            }
                                        })
                                        .setOnHideListener(new OnHideAlertListener() {
                                            @Override
                                            public void onHide() {
                                                new ToastDialog(getContext()).setText("hide").show();
                                            }
                                        })
                                        .show();
                            }
                        });

                        //增加按钮
                        Button btnAddButton = (Button) findComponentById(ResourceTable.Id_btn_alert8);
                        btnAddButton.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setDuration(5000)
                                        .addButton("YES", 0xffffffff, 0xffF99143, 200, 60, new Component.ClickedListener() {
                                            @Override
                                            public void onClick(Component component) {
                                                new ToastDialog(getContext()).setText("yes").show();
                                            }
                                        })
                                        .addButton("NO", 0xffffffff, 0xffF99143, 200, 60, new Component.ClickedListener() {
                                            @Override
                                            public void onClick(Component component) {
                                                new ToastDialog(getContext()).setText("no").show();
                                            }
                                        })
                                        .show();
                            }
                        });

                        //自定义布局
                        Button btnCustomLayout = (Button) findComponentById(ResourceTable.Id_btn_alert9);
                        btnCustomLayout.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer, ResourceTable.Layout_customer_layout)
                                        .setDuration(5000)
                                        .show();
                            }
                        });

                        //VERBOSE
                        Button btnVerbose = (Button) findComponentById(ResourceTable.Id_btn_alert10);
                        btnVerbose.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("The alert scales to accommodate larger bodies of text. " +
                                                "The alert scales to accommodate larger bodies of text. " +
                                                "The alert scales to accommodate larger bodies of text.")
                                        .show();
                            }
                        });

                        //中间弹窗
                        Button btnCenter = (Button) findComponentById(ResourceTable.Id_btn_alert11);
                        btnCenter.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setContentGravity(DependentLayout.LayoutConfig.CENTER_IN_PARENT)
                                        .show();
                            }
                        });

                        //底部弹窗
                        Button btnBottom = (Button) findComponentById(ResourceTable.Id_btn_alert12);
                        btnBottom.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .setContentGravity(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM)
                                        .show();
                            }
                        });

                        //带右图标弹窗
                        Button btnWithRightIcon = (Button) findComponentById(ResourceTable.Id_btn_alert13);
                        btnWithRightIcon.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setCustomRightIconBig(ResourceTable.Media_custom_icon_big)
                                        .setCustomRightIconSmall(ResourceTable.Media_custom_icon_small)
                                        .setEnableCustomRightIconAnim(true)
                                        .setEnableRightIcon(true)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .show();
                            }
                        });

                        //只有右图标弹窗
                        Button btnOnlyRightIcon = (Button) findComponentById(ResourceTable.Id_btn_alert14);
                        btnOnlyRightIcon.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setCustomRightIconBig(ResourceTable.Media_custom_icon_big)
                                        .setCustomRightIconSmall(ResourceTable.Media_custom_icon_small)
                                        .setEnableCustomRightIconAnim(true)
                                        .setEnableOnlyRightIcon(true)
                                        .setEnableRightIcon(true)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .show();
                            }
                        });

                        //右上角图标弹窗
                        Button btnRightOnTopIcon = (Button) findComponentById(ResourceTable.Id_btn_alert15);
                        btnRightOnTopIcon.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setCustomRightIconBig(ResourceTable.Media_custom_icon_big)
                                        .setCustomRightIconSmall(ResourceTable.Media_custom_icon_small)
                                        .setRightIconTop()
                                        .setEnableCustomRightIconAnim(true)
                                        .setEnableOnlyRightIcon(true)
                                        .setEnableRightIcon(true)
                                        .setEnableIconAnim(true)
                                        .setText("The alert scales to accommodate larger bodies of text. " +
                                                "The alert scales to accommodate larger bodies of text. " +
                                                "The alert scales to accommodate larger bodies of text.")
                                        .show();
                            }
                        });

                        //只有文本
                        Button btnOnlyText = (Button) findComponentById(ResourceTable.Id_btn_alert16);
                        btnOnlyText.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setText("Alert text...")
                                        .show();
                            }
                        });

                        //带铃声
                        Button btnSound = (Button) findComponentById(ResourceTable.Id_btn_alert17);
                        btnSound.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                copyMp3File();
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setSoundUri(Uri.parse(getFilesDir() + File.separator + "ringtone.mp3"))
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .show();
                            }
                        });

                        //SWIPE DISMISS
                        Button btnSwipeDismiss = (Button) findComponentById(ResourceTable.Id_btn_alert18);
                        btnSwipeDismiss.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .enableSwipeToDismiss()
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .show();
                            }
                        });

                        //自定义标题内容颜色
                        Button btnCustomTextColor= (Button) findComponentById(ResourceTable.Id_btn_alert19);
                        btnCustomTextColor.setClickedListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Alert alert =  Alerter.create(MainAbilitySlice.this, componentContainer)
                                        .setEnableIconAnim(true)
                                        .setTitle("Alert Title")
                                        .setText("Alert text...")
                                        .show();
                                alert.getTitle().setTextColor(new Color(0xffF99143));
                                alert.getText().setTextColor(new Color(0xffcc0000));
                            }
                        });
```

```xml
             <?xml version="1.0" encoding="utf-8"?>
             <DependentLayout
                     xmlns:ohos="http://schemas.huawei.com/res/ohos"
                     ohos:id="$+id:dl_group"
                     ohos:height="match_parent"
                     ohos:width="match_parent"
                     ohos:background_element="#fff"
             >


                 <DependentLayout
                         ohos:id="$+id:titlebar"
                         ohos:top_margin="50vp"
                         ohos:height="50vp"
                         ohos:width="match_parent"
                         ohos:background_element="#2c9cac"
                 >
                     <Text
                             ohos:text_color="#fff"
                             ohos:text="Alert"
                             ohos:text_alignment="horizontal_center"
                             ohos:center_in_parent="true"
                             ohos:align_parent_left="true"
                             ohos:left_margin="20vp"
                             ohos:text_size="20fp"
                             ohos:height="match_content"
                             ohos:width="match_content"/>
                 </DependentLayout>


                 <ScrollView
                         ohos:below="$id:titlebar"
                         ohos:layout_alignment="horizontal_center"
                         ohos:top_margin="10vp"
                         ohos:height="match_parent"
                         ohos:width="match_parent">


                     <DependentLayout
                             ohos:height="match_content"
                             ohos:width="match_parent">

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:id="$+id:btn_alert1"
                                 ohos:text="DEFAULT ALERT"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:top_margin="10vp"
                                 ohos:text_size="18fp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:id="$+id:btn_alert2"
                                 ohos:below="$+id:btn_alert1"
                                 ohos:top_margin="10vp"
                                 ohos:text="ON CLICK ALERT"
                                 ohos:height="50vp"
                                 ohos:text_size="18fp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>


                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:id="$+id:btn_alert3"
                                 ohos:below="$+id:btn_alert2"
                                 ohos:top_margin="10vp"
                                 ohos:text_size="18fp"
                                 ohos:text="CUSTOM ICON ALERT"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>


                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert3"
                                 ohos:id="$+id:btn_alert4"
                                 ohos:top_margin="10vp"
                                 ohos:text="COLOURED ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert4"
                                 ohos:id="$+id:btn_alert5"
                                 ohos:top_margin="10vp"
                                 ohos:text="PROGRESS ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert5"
                                 ohos:id="$+id:btn_alert6"
                                 ohos:top_margin="10vp"
                                 ohos:text="FONT ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert6"
                                 ohos:id="$+id:btn_alert7"
                                 ohos:top_margin="10vp"
                                 ohos:text="CALLBACK ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert7"
                                 ohos:id="$+id:btn_alert8"
                                 ohos:top_margin="10vp"
                                 ohos:text="WITH BUTTON ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert8"
                                 ohos:id="$+id:btn_alert9"
                                 ohos:top_margin="10vp"
                                 ohos:text="CUSTOM LAYOUT ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert9"
                                 ohos:id="$+id:btn_alert10"
                                 ohos:top_margin="10vp"
                                 ohos:text="VERBOSE ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert10"
                                 ohos:id="$+id:btn_alert11"
                                 ohos:top_margin="10vp"
                                 ohos:text="CENTER ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert11"
                                 ohos:id="$+id:btn_alert12"
                                 ohos:top_margin="10vp"
                                 ohos:text="BOTTOM ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert12"
                                 ohos:id="$+id:btn_alert13"
                                 ohos:top_margin="10vp"
                                 ohos:text="WITH RIGHT ICON"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert13"
                                 ohos:id="$+id:btn_alert14"
                                 ohos:top_margin="10vp"
                                 ohos:text="WITH ONLY RIGHT ICON"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert14"
                                 ohos:id="$+id:btn_alert15"
                                 ohos:top_margin="10vp"
                                 ohos:text="RIGHT ICON ON TOP"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert15"
                                 ohos:id="$+id:btn_alert16"
                                 ohos:top_margin="10vp"
                                 ohos:text="TEXT ONLY ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert16"
                                 ohos:id="$+id:btn_alert17"
                                 ohos:top_margin="10vp"
                                 ohos:text="SOUND ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert17"
                                 ohos:id="$+id:btn_alert18"
                                 ohos:top_margin="10vp"
                                 ohos:text="SWIPE DISMISS ALERT"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>

                         <Button
                                 ohos:background_element="$graphic:background_element"
                                 ohos:text_color="#fff"
                                 ohos:below="$+id:btn_alert18"
                                 ohos:id="$+id:btn_alert19"
                                 ohos:top_margin="10vp"
                                 ohos:text="CUSTOM TEXT COLOR"
                                 ohos:bottom_margin="20vp"
                                 ohos:text_size="18fp"
                                 ohos:height="50vp"
                                 ohos:left_margin="30vp"
                                 ohos:right_margin="30vp"
                                 ohos:width="match_parent"/>
                     </DependentLayout>
                 </ScrollView>

             </DependentLayout>
```

## Licence
See the LICENSE file for license rights and limitations (MIT).

Copyright 2017 Tapadoo, Dublin.

